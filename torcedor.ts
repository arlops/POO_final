import { Ingresso } from './ingresso'

export class Torcedor{
    private nome: string;
    private idade: number;
    private ingresso: Ingresso;
    private musica: string;

    constructor(nome: string, idade: number, public time: string, ingresso: Ingresso){
        this.nome = nome;
        this.idade = idade;
        this.time = time;
        this.ingresso = ingresso;
    }

    public setMusica(musica: string){
        this.musica = musica;
    }
}
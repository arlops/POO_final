export class Ingresso{
    private clube: string;
    private preco: number;
    private static qtdIngresso: number=0;

    public comprarIngresso(valor: number): number{
        if (this.preco >= valor){
            Ingresso.qtdIngresso += 1;
            return Ingresso.qtdIngresso;
        }else{
            return 0;
        }
    }

    public getPreco(): number{
        return this.preco;
    }
}